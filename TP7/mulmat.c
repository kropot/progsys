#include<stdio.h>

void saisir_matrice(long double matrice[3][3]) {
    int i, j;
    printf("Saisir les éléments de la matrice :\n");
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            scanf("%Lf", &matrice[i][j]);
        }
    }
}

void afficher_matrice(long double matrice[3][3]) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%2.2Lf ", matrice[i][j]);
        }
        printf("\n");
    }
}

void multiplier_matrice(long double mat1[3][3], long double mat2[3][3], \
		long double resultat[3][3]) {
	// à vous
	int i, j, k ;
	for (i = 0; i < 3; i++) {
	    for (j = 0; j < 3; j++) {
		resultat[i][j] = 0 ;
	        for (k = 0; k < 3; k++) {
		    resultat[i][j] += mat1 [i][k] * mat2 [k][j];
		}
	    }
	}
}

int main() {
    long double matrice1[3][3];
    long double matrice2[3][3];
    long double produit[3][3];

    saisir_matrice(matrice1);
    saisir_matrice(matrice2);
    afficher_matrice(matrice1);
    afficher_matrice(matrice2);

    // ce qui doit marcher :
    multiplier_matrice(matrice1, matrice2, produit);
    afficher_matrice(produit);

    return 0;
}

#include<stdio.h>

// Chiffrement avec l'algorithme de César
// en Python : chr( ( ord(c) - ord('a') + k ) % 26 + ord('a') )


void cesar(char *message, int key, char *crypted) {
    char *p;
    int i = 0;
    // parcour de la chaîne
    for (p=message; *p; p++) {
	    printf("Traitement de %c\n", *p);
	    // TODO: remplir le tableau de char crypted
	    crypted[i] = *p + key ;
	    i++ ;
    }
    // TODO: ajouter le zéro final pour indiquer la fin de chaîne
    crypted[i + 1] = 0 ;
}

int main() {
    char message[50];
    printf("saisir le message sans espace:\n");
    scanf("%s", message);
    char crypted[50];
    int key = 0;
    printf("saisir une clé :\n");
    scanf("%d", &key);

    cesar(message, key, crypted);
    printf("%s\n", crypted);
}

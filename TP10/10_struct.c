#include<stdio.h>
#include<math.h>

typedef struct point point;
struct point {
	char *label;
	long double x,y;
};

void affiche_point(point p) {
	printf("%s est aux coordonnées (%.2Lf, %.2Lf)\n", p.label, p.x, p.y);
	printf("le vecteur op est de %.2Lf:\n", sqrtl((p.x*p.x)+(p.y*p.y))) ;
}

int main() {
	point p1 = { "Le point A", 10.0, 3.14 };
	point p2;

	p2.label = "Le point B";
	p2.x = 42.0;
	p2.y = 12.1;

	affiche_point(p1);
	affiche_point(p2);

	return 0;
}

